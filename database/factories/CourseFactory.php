<?php

use Faker\Generator as Faker;

$factory->define(App\Course::class, function (Faker $faker) {
    return [
        'code' => strtoupper($faker->lexify('???')) . $faker->numberBetween(101, 999),
        'title' => $faker->sentence(3),
        'description' => $faker->paragraph
    ];
});
