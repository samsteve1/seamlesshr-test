<?php

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('register', 'RegisterController@action');
    Route::post('login', 'LoginController@action');
});

Route::get('get-courses', 'CourseController@index');
Route::get('export-courses', 'CourseExportController@action');
Route::post('create-courses', 'CourseController@store');


Route::group(['prefix' => 'courses', 'middleware' => ['auth']], function () {
    Route::get('/', 'MyCoursesController@action');
    Route::post('enroll', 'EnrollController@action');
    
});