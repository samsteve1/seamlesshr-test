<?php

namespace App\Http\Controllers;

use App\CoursesExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CourseExportController extends Controller
{
    public function action()
    {
         Excel::store( new CoursesExport, 'courses.xlsx');
         return response('Courses exported to excel', 200);
    }
}
