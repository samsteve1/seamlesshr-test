<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCoursesResource;
use Illuminate\Http\Request;

class MyCoursesController extends Controller
{
    public function action()
    {
        $courses = auth()->user()->courses;

        if($courses->count()) {
            return UserCoursesResource::collection($courses);
        }
        return response('You have not registered for any course yet.', 200);
        
    }
}
