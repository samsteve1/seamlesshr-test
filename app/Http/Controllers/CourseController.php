<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\CreateCourseRequest;
use App\Http\Resources\CourseResource;
use App\Jobs\CreateCourses;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index()
    {
        
        return CourseResource::collection(Course::get());
    }
    public function store(CreateCourseRequest $request)
    {
        // the defult number of courses is 50, but can be specified from the request
        $number = $request->has('num') ? $request->num : 50;

        $this->dispatch(new CreateCourses($number));
    }
}
