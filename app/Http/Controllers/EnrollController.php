<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\EnrollRequest;
use App\Http\Resources\CourseResource;
use Illuminate\Http\Request;

class EnrollController extends Controller
{
    public function action(EnrollRequest $request)
    {
        $courses = $request->courses;
        
        $courses = Course::findMany($courses);

        auth()->user()->courses()->sync($courses);

        return CourseResource::collection($courses)->additional([
            'message' => auth()->user()->name . ' enrolled'
        ]);
    }
}
