<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Course;

class CreateCourses implements ShouldQueue
{
    protected $number;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct($number)
    {
        $this->number = $number;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       // dd((int)$this->number);
        factory(Course::class, (int)$this->number)->create();
    }
}
