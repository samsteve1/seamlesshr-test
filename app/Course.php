<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'code',
        'title',
        'description'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_courses')->withPivot('created_at');
    }
}
